package ru.ekfedorov.tm.constant;

public interface SiteConst {

    String site = "http://localhost:8080";

    String projects_address = "/api/projects";

    String tasks_address = "/api/tasks";

}
