package ru.ekfedorov.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ekfedorov.tm.comparator.*;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    DATE_START("Sort by date start", ComparatorByDateStart.getInstance()),
    DATE_FINISH("Sort by date finish", ComparatorByDateFinish.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    @NotNull
    private final Comparator comparator;

    @NotNull
    private final String displayName;

    Sort(@NotNull String displayName, @NotNull Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    @NotNull
    public Comparator getComparator() {
        return comparator;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

}
