package ru.ekfedorov.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ekfedorov.tm.model.Project;


public interface IProjectRepository extends JpaRepository<Project, String> {
}
