package ru.ekfedorov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.ekfedorov.tm.model.Task;


public interface ITaskRepository extends JpaRepository<Task, String> {

    @Modifying
    @Query("UPDATE Task e SET e.projectId = :projectId WHERE e.id = :taskId")
    void bindTaskByProjectId(
            @Param("projectId") @NotNull String projectId,
            @Param("taskId") @NotNull String taskId
    );

    @Modifying
    @Query("UPDATE Task e SET e.projectId = NULL WHERE e.id = :id")
    void unbindTaskFromProjectId(
            @Param("id") @NotNull String id
    );

}