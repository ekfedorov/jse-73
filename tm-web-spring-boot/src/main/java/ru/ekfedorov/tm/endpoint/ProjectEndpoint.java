package ru.ekfedorov.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.ekfedorov.tm.api.endpoint.IProjectEndpoint;
import ru.ekfedorov.tm.api.service.IProjectService;
import ru.ekfedorov.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
@RestController
@RequestMapping("/api/projects")
public class ProjectEndpoint implements IProjectEndpoint {

    @Autowired
    private IProjectService projectService;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return projectService.findAll();
    }

    @Override
    @WebMethod
    @GetMapping("/find/{id}")
    public Project find(@PathVariable("id") final String id) {
        return projectService.findById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/create")
    public Project create(@RequestBody final Project project) {
        projectService.add(project);
        return project;
    }

    @Override
    @WebMethod
    @PostMapping("/createAll")
    public List<Project> createAll(@RequestBody final List<Project> projects) {
        projectService.addAll(projects);
        return projects;
    }

    @Override
    @WebMethod
    @PutMapping("/save")
    public Project save(@RequestBody final Project project) {
        projectService.add(project);
        return project;
    }

    @Override
    @WebMethod
    @PutMapping("/saveAll")
    public List<Project> saveAll(@RequestBody final List<Project> projects) {
        projectService.addAll(projects);
        return projects;
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") final String id) {
        projectService.removeById(id);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        projectService.clear();
    }

}
