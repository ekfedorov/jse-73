package ru.ekfedorov.tm.api.endpoint;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.ekfedorov.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebService;
import java.util.List;

@WebService
public interface IProjectEndpoint {

    @WebMethod
    @GetMapping("/findAll")
    List<Project> findAll();

    @WebMethod
    @GetMapping("/find/{id}")
    Project find(@PathVariable("id") String id);

    @WebMethod
    @PostMapping("/create")
    Project create(@RequestBody Project project);

    @WebMethod
    @PostMapping("/createAll")
    List<Project> createAll(@RequestBody List<Project> projects);

    @WebMethod
    @PostMapping("/save")
    Project save(@RequestBody Project project);

    @WebMethod
    @PostMapping("/saveAll")
    List<Project> saveAll(@RequestBody List<Project> projects);

    @WebMethod
    @PostMapping("/delete/{id}")
    void delete(@PathVariable("id") String id);

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll();

}
