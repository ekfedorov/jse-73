package ru.ekfedorov.tm.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.event.ConsoleEvent;
import ru.ekfedorov.tm.listener.AbstractTaskListener;
import ru.ekfedorov.tm.endpoint.*;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.util.TerminalUtil;

import java.util.List;

@Component
public final class TaskFindAllByProjectIdListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Find al lby project id.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "find-all-by-project-id";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@taskFindAllByProjectIdListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new NullObjectException();
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[TASK LIST BY PROJECT]");
        System.out.println("ENTER ID:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final List<Task> tasks = taskEndpoint.findAllByProjectId(session, id);
        if (tasks.isEmpty()) {
            System.out.println("--- There are no tasks ---");
            return;
        }
        int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

}
